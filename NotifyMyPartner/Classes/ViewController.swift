//
//  ViewController.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 5/29/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit

let kRegistrationDone = "RegistrationDone"
let kResponseErrorMsg = "Some error occured, please try again later."
let kUserEmail = "userEmail"
let kSession = "session"
let kToken = "token"

class ViewController: UIViewController, UIAlertViewDelegate, NSURLConnectionDelegate
{
    var receivedData = NSMutableData()
    var results = NSDictionary()
    
    // Outlets
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var progressIndicatingView: UIView!
    
    // MARK - ViewLifeCycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK - IBActions
    
    @IBAction func termsOfServicePressed(sender: AnyObject)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let webViewController = storyboard.instantiateViewControllerWithIdentifier("webViewIdentifier") as! UIViewController
        self.presentViewController(webViewController, animated: true, completion: nil)
    }
    
    @IBAction func dismissKeyboardPressed(sender: AnyObject)
    {
        emailField.resignFirstResponder()
        passwordField.resignFirstResponder()
    }
    
    @IBAction func SignUpPressed(sender: AnyObject)
    {
        emailField.text = emailField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        passwordField.text = passwordField.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        
        var alertMessage : NSString = "";
        
        if emailField.text.isEmpty
        {
            alertMessage = "Please enter your email Id."
        }
        else if !isValidEmail(emailField.text)
        {
            alertMessage = "Please enter valid email Id."
        }
        else if passwordField.text.isEmpty
        {
            alertMessage = "Please enter password."
        }

        if alertMessage.length > 0
        {
            Helper.showAlertDialog("Error", alertMessage: alertMessage)
            return
        }
        
        if Reachability.reachabilityForInternetConnection().isReachable() == false
        {
            Helper.showInternetConnectionWarning()
            return
        }
        
        // start connection
        var urlString = "http://www.xymob.com/s360/account/create"
        var url = NSURL(string: urlString)
        var body = "email=\(emailField.text)&password=\(passwordField.text)&provider=app"
        var request = NSMutableURLRequest(URL: url!)
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.HTTPMethod = "POST"
        request.HTTPBody = body.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)?.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        let devicePlatform = Helper.devicePlatformInformation() as String
        let deviceId = Helper.deviceId() as String
        let appversion = Helper.appVersion() as String
        
        // set request headers
        request .setValue(devicePlatform, forHTTPHeaderField: "platform")
        request.setValue(deviceId, forHTTPHeaderField: "device")
        request.setValue(appversion, forHTTPHeaderField: "app-version")
        request.setValue("1.0", forHTTPHeaderField: "version")
        request.setValue("QuantifiedPlace", forHTTPHeaderField: "app-code")// TO_DO
        
        NSLog("all headers %@",request.allHTTPHeaderFields!)
        self.receivedData = NSMutableData()
        
        // making request
        var connection = NSURLConnection(request: request, delegate: self, startImmediately: true)
        
        progressIndicatingView.hidden = false
        self.view.endEditing(true)
    }
    
    // MARK - User Defined Functions
    
    func isValidEmail(emailStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(emailStr)
    }
    
    // MARK - NSURLConnection Delegate
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse)
    {
        NSLog("didReceiveResponse")
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData)
    {
        NSLog("didReceiveData")
        
        // Appending data
        self.receivedData.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection)
    {
        NSLog("connectionDidFinishLoading %@", self.receivedData)
        progressIndicatingView.hidden = true
        
        if self.receivedData.length > 0
        {
            results = NSJSONSerialization.JSONObjectWithData((self.receivedData), options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary
        }
        
        NSLog("response %@", results)

        if results.objectForKey("status")?.integerValue != 200
        {
            var errMsg = kResponseErrorMsg
            
            if results.objectForKey("error") != nil &&
            results.objectForKey("error")?.objectForKey("msg") != nil
            {
                errMsg = results.objectForKey("error")?.objectForKey("msg") as! String
            }
            
            var alert = UIAlertController(title: "Error", message: errMsg, preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if results.objectForKey("email") != nil
        {
            defaults.setObject(results.objectForKey("email"), forKey: kUserEmail)
        }
        
        if results.objectForKey(kSession) != nil
        {
            defaults.setObject(results.objectForKey(kSession), forKey: kSession)
        }
        
        if results.objectForKey(kToken) != nil
        {
            defaults.setObject(results.objectForKey(kToken), forKey: kToken)
        }

        defaults.setBool(true, forKey: kRegistrationDone)
        
        defaults.synchronize()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let locationPermissionController = storyboard.instantiateViewControllerWithIdentifier("LocationPermision") as! LocationPermissionScreen
        appDelegate.window?.rootViewController = locationPermissionController
    }
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError)
    {
        NSLog("didFailWithError %@", error)
        
        var alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        
        progressIndicatingView.hidden = true
    }
}


