//
//  LocationPermissionScreen.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 5/29/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class LocationPermissionScreen : UIViewController, CLLocationManagerDelegate
{
    // Outlets
    @IBOutlet var notificationLabel: UILabel!
    
    // MARK - view Life Cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool)
    {
        checkAuthorizationStatus()
    }

    // MARK - User defined Functions
    
    func checkAuthorizationStatus()
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        switch CLLocationManager.authorizationStatus()
        {
        case CLAuthorizationStatus.AuthorizedAlways:
            NSLog("CLAuthorizationStatus > Authorized")
            if var label = self.notificationLabel
            {
                self.notificationLabel.text = "Thanks for allowing us to use your location."
            }
        case CLAuthorizationStatus.NotDetermined:
            if var label = self.notificationLabel
            {
                self.notificationLabel.text = "We need your location permission, in order to send you email reports for the places you visit in the whole week."
            }
            appDelegate.manager.requestAlwaysAuthorization()
        case CLAuthorizationStatus.AuthorizedWhenInUse, CLAuthorizationStatus.Restricted, CLAuthorizationStatus.Denied:
            let alertController = UIAlertController(title: "Background Location Access Disabled",
                message: "In order to send you email reports for the places you visit, we need to access your location.", preferredStyle: UIAlertControllerStyle.Alert)
            
            let cancelAction = UIAlertAction(title: "Cancel",
                style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
                    if var label = self.notificationLabel
                    {
                        self.notificationLabel.text = "You have not given permission to access your Location Services, hence we can't allow you to use this app, please go to Settings App > Privacy > Location and select Always for this app."
                    }
            })
            alertController.addAction(cancelAction)
            
            let openAction = UIAlertAction(title: "Open Settings",
                style: UIAlertActionStyle.Default,
                handler: { (action) -> Void in
                    if let url = NSURL(string: UIApplicationOpenSettingsURLString)
                    {
                        UIApplication.sharedApplication().openURL(url)
                    }
            })
            alertController.addAction(openAction)
            appDelegate.window?.rootViewController?.presentViewController(alertController, animated: true, completion: nil)
        }
    }
}