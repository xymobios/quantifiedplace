//
//  ConfirmationScreen.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 6/1/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import Foundation
import UIKit

class ConfirmationScreen: UIViewController
{
    @IBOutlet var messageToDisplayLabel: UILabel!
    @IBOutlet var pauseOrRestartButton: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func pauseOrRestartPressed(sender: AnyObject)
    {
        if pauseOrRestartButton.tag == 100
        {
            pauseOrRestartButton.setTitle("Re-start", forState: UIControlState.Normal)
            pauseOrRestartButton.tag = 200
            // write code to register Recipe TO_DO
        }
        else if pauseOrRestartButton.tag == 200
        {
            pauseOrRestartButton.setTitle("Pause", forState: UIControlState.Normal)
            pauseOrRestartButton.tag = 100
            // write code to unregister recipe TO_DO
        }
    }
}