//
//  Helper.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 6/2/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit
import AdSupport

public class Helper: NSObject
{
    // device token
    public class func token() -> NSString
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        let token =  defaults.objectForKey("token") as! String
        return token
    }
    
    // session
    public class func sessionValue() -> NSString
    {
        let defaults = NSUserDefaults.standardUserDefaults()
        let session =  defaults.objectForKey("session") as! String
        return session
    }
    
    // device platform info
    public class func devicePlatformInformation() ->NSString
    {
        let device = UIDevice.currentDevice()
        let platform = "\(device.systemName) \(device.systemVersion)"
        return platform
    }
    
    // device UUID
    public class func deviceId() -> NSString
    {
        if ASIdentifierManager.sharedManager().respondsToSelector(Selector("advertisingIdentifier"))
        {
            return ASIdentifierManager.sharedManager().advertisingIdentifier.UUIDString
        }
        
        return ""
    }
    
    // application version
    public class func appVersion() -> NSString
    {
        return NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleShortVersionString")as! String
    }
    
    // Appl build Number
    public class func buildNumber() -> NSString
    {
        return NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleVersion") as! String
    }
   
    /* It has crash
    // appplication name
    public class func appName() -> NSString
    {
        return NSBundle.mainBundle().objectForInfoDictionaryKey("CFBundleDisplayName") as String
    }*/
    
    // Internet connection warning
    public class func showInternetConnectionWarning()
    {
        showAlertDialog("Warning", alertMessage: "We are unable to connect to the network. Please make sure that your device is connected to the internet.")
    }
    
    // show alert dialog
    public class func showAlertDialog(alertTitle: NSString, alertMessage: NSString)
    {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let title = alertTitle as String
        let msg = alertMessage as String
        
        var alert = UIAlertController(title: title,
            message: msg,
            preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        appDelegate.window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
    }
}
