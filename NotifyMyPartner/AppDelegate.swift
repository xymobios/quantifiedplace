//
//  AppDelegate.swift
//  NotifyMyPartner
//
//  Created by Nidhi Sharma on 5/29/15.
//  Copyright (c) 2015 Nidhi. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
// import SenseSDK TO_DO

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate/*,SenseApiDelegate*/  {

    var window: UIWindow?
    let manager  = CLLocationManager()
    
    var receivedData = NSMutableData()
    var responseDict = NSDictionary()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        manager.delegate = self
        setRootController()
        self.window?.makeKeyAndVisible()
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let defaults = NSUserDefaults.standardUserDefaults()
        if defaults.boolForKey("RegistrationDone")
        {
            if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedAlways
            {
                let locationPermissionController = storyboard.instantiateViewControllerWithIdentifier("LocationPermision") as! LocationPermissionScreen
                self.window?.rootViewController = locationPermissionController
                locationPermissionController.viewDidAppear(false)
            }
            else
            {
                let confirmationScreen = storyboard.instantiateViewControllerWithIdentifier("confirmationScreenId") as! UIViewController
                self.window?.rootViewController = confirmationScreen
            }
        }
    }

    func applicationDidBecomeActive(application: UIApplication) {
       
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        //SenseApi.saveState() TO_DO (Uncomment Me)
    }

    // MARK - CLLocation MAnager Delegate Methods
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        if status == CLAuthorizationStatus.AuthorizedAlways
        {
            NSLog("CLAuthorizationStatus > location is authorized")
            
            let defaults = NSUserDefaults.standardUserDefaults()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let confirmationScreen = storyboard.instantiateViewControllerWithIdentifier("confirmationScreenId") as! UIViewController
            self.window?.rootViewController = confirmationScreen
        }
        else if status == CLAuthorizationStatus.AuthorizedWhenInUse
        {
            NSLog("CLAuthorizationStatus > AuthorizedWhenInUse")
        }
        else if status == CLAuthorizationStatus.NotDetermined
        {
            NSLog("CLAuthorizationStatus > NotDetermined")
        }
        else if status == CLAuthorizationStatus.Denied
        {
            NSLog("CLAuthorizationStatus > Denied")
        }
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
    {
        NSLog("didUpdateLocations: %@", locations)
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!)
    {
        NSLog("didFailWithError : %@", error)
    }
    
    // MARK - User Defined Functions
    
    func setRootController()
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if defaults.boolForKey("RegistrationDone")
        {
            if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.AuthorizedAlways
            {
                let locationPermissionController = storyboard.instantiateViewControllerWithIdentifier("LocationPermision") as! UIViewController
                self.window?.rootViewController = locationPermissionController
            }
            else
            {
                let confirmationScreen = storyboard.instantiateViewControllerWithIdentifier("confirmationScreenId") as! UIViewController
                self.window?.rootViewController = confirmationScreen
            }
        }
        else
        {
            let registrationScreen = storyboard.instantiateViewControllerWithIdentifier("RegistrationController") as! UIViewController
            self.window?.rootViewController = registrationScreen
        }
    }
    
    // MARK - SenseAPI Related
    
    func registerRecipeWithSenseSDK()
    {
        NSLog( "Registering Sense SDk with time window")
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let confirmationScreen = storyboard.instantiateViewControllerWithIdentifier("confirmationScreenId") as! UIViewController
        self.window?.rootViewController = confirmationScreen
        
        // Register recipe here with Sense360 SDK TO_DO
        
        // Setup
        /*SenseApi.apiKey = "API Key" // TO_DO (Set API Key Here)
        
        let workTrigger = PersonalizedTriggerBuilder()
        .set(place: PersonalizedPlaceType.Work)
        .hasExit()
        .build()
        
        let recipe = Recipe(name: "WorkExitRecipe",
        trigger: workTrigger,
        window: TimeWindow(fromHour: startDateComponents.hour, toHour: endDateComponents.hour))
        
        // Registering a recipe and delegate
        let results = SenseApi.register(recipe: recipe, delegate: myDelegate)
        if results.successful
        {
            NSLog("recipe registration successful")

            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let confirmationScreen = storyboard.instantiateViewControllerWithIdentifier("confirmationScreenId") as UIViewController
            appDelegate.window?.rootViewController = confirmationScreen

        }
        else if let errors = result.errors
        {
            NSLog("Registration unsucessful, see errors below")
            for error in errors
            {
                NSLog("message: \(error.message)")
            }
            Helper.showAlertDialog("Error", alertMessage: "Some error occured, please try again later.")
        }*/
    }
    
    // MARK - Sense API Delegate Methods
    
    /*func onTriggerFired(args: TriggerFiredArgs)
    {
        NSLog("Triggered \(args.recipe.trigger.customIdentifier) at time \(args.timestamp)")
    
        // start connection
        var urlString = "http://www.xymob.com/s360/trigger/message" // CHECK_ME
        var url = NSURL(string: urlString)

        let defaults = NSUserDefaults.standardUserDefaults()
        
        var body = "type=2&category=Work&message=\(message)&phone=\(phone)&timestamp=\(args.timestamp)"
        var request = NSMutableURLRequest(URL: url!)
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData
        request.HTTPMethod = "POST"
        request.HTTPBody = body.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)?.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        // set request headers
        request .setValue(Helper.devicePlatformInformation(), forHTTPHeaderField: "platform")
        request.setValue(Helper.deviceId(), forHTTPHeaderField: "device")
        request.setValue(Helper.appVersion(), forHTTPHeaderField: "app-version")
        request.setValue("1.0", forHTTPHeaderField: "version")
        request.setValue("NotifyMyPartner", forHTTPHeaderField: "app-code")// TO_DO
    
    if let session = Helper.sessionValue() as String?
    {
    request.setValue(Helper.sessionValue(), forHTTPHeaderField: "session")
    }
    
    if let token = Helper.token() as String?
    {
    request.setValue(Helper.token(), forHTTPHeaderField: "token")
    }
    
    self.receivedData = NSMutableData()
    
    // making request
    var connection = NSURLConnection(request: request, delegate: self, startImmediately: true)
    }*/
    
    // MARK - NSURLConnection Delegate
    
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse)
    {
        NSLog("didReceiveResponse")
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData)
    {
        NSLog("didReceiveData")
        
        // Appending data
        self.receivedData.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection)
    {
        NSLog("connectionDidFinishLoading %@", self.receivedData)
        
        if self.receivedData.length > 0
        {
            responseDict = NSJSONSerialization.JSONObjectWithData((self.receivedData), options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSDictionary
        }
        
        NSLog("response %@", responseDict)
    }
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError)
    {
        NSLog("didFailWithError")
    }
}

